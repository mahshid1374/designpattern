package fasade;

public class Test {
    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.run();
    }
}
