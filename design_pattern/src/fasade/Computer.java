package fasade;
//is fasade
public class Computer {
    private CPU cpu;
    private Memory memory;
    private HardDrive hardDrive;

    public Computer() {
        this.cpu = cpu;
        this.memory = memory;
        this.hardDrive = hardDrive;
    }
    public void run(){
        cpu.processData();
        memory.loadData();
        hardDrive.readData();
    }
}
