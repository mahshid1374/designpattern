package singletone.violate_singleton.clone;

public class Test {
    public static void main(String[] args) throws CloneNotSupportedException {

        //false
        //Singleton obj = new Singleton();
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();

        print("s1",s1);
        print("s2",s2);

        Singleton s3 =(Singleton)s2.clone();
        print("s3",s3);

    }
    static  void print(String name,Singleton obj){
        System.out.println(String.format("Object : %s,Hashcode : %d"));
    }
}
