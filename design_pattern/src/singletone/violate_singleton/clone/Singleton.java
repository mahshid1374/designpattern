package singletone.violate_singleton.clone;

public class Singleton implements Cloneable{
    private static Singleton soleInstance = new Singleton();
    private Singleton(){}
    public static Singleton getInstance(){
        return soleInstance;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
