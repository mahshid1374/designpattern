package singletone.violate_singleton.reflection;

public class Singleton {
    private static Singleton soleInstance = new Singleton();
    private Singleton(){}
    public static Singleton getInstance(){
        return soleInstance;
    }
}
