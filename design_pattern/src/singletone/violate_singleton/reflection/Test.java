package singletone.violate_singleton.reflection;

import java.lang.reflect.Constructor;

public class Test {
    public static void main(String[] args) throws Exception{
        //not true
        //Singleton obj = new Singleton();

        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();
        print("s1",s1);
        print("s2",s2);

        //Reflection
        Class a = Class.forName("singletone.violate_singleton.reflection.Singleton");
        //get default constructor
        Constructor<Singleton> ctor = a.getDeclaredConstructor();
        ctor.setAccessible(true);
        Singleton s3 =ctor.newInstance();
        print("s3",s3);
    }
    static void print(String name,Singleton obj){
        System.out.println(String.format("Object : %s,Hashcode : %d",name,obj.hashCode()));
    }
}
