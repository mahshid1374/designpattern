package singletone.violate_singleton.multi_thread_lazy;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test {
    static void useSingleton(){
        Singleton s1 =Singleton.getInstance();
        print("s1",s1);
    }
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.submit(Test::useSingleton);
        service.submit(Test::useSingleton);
        service.shutdown();
    }

    static void print(String name,Singleton object){
        System.out.println(String.format("Object : %s,Hashcode : %d",name,object.hashCode()));
    }
}
