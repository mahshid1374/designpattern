package singletone.violate_singleton.multi_thread_lazy;

public class Singleton {
    private static Singleton soleInstance=null;
    private Singleton(){}
    public static Singleton getInstance(){
        if(soleInstance == null)
        {
            soleInstance = new Singleton();
        }
        return soleInstance;
    }
}
