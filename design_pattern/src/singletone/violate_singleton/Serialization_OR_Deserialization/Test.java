package singletone.violate_singleton.Serialization_OR_Deserialization;

import java.io.*;

public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Singleton s1=Singleton.getInstance();
        Singleton s2=Singleton.getInstance();
        print("s1",s1);
        print("s2",s2);
        //Serialization ex.
        ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream("C:\\Users\\mahshid\\Desktop\\my_final_project\\design_pattern\\src\\singletone\\Serialization_OR_Deserialization\\a.txt"));
        oos.writeObject(s2);

        //Deserialization
        ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream("C:\\Users\\mahshid\\Desktop\\my_final_project\\design_pattern\\src\\singletone\\Serialization_OR_Deserialization\\a.txt"));
        Singleton s3 = (Singleton)ois.readObject();
        print("s3",s3);
    }

    static void print(String name,Singleton obj){
        System.out.println(String.format("Object : %s , Hashcode : %d" ,name,obj.hashCode()));
    }
}
