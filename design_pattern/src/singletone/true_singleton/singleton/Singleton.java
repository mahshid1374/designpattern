package singletone.violate_singleton.true_singleton.singleton;

public class Singleton {

    private static Singleton soleInstance=new Singleton();

    //nobody cannot create a new instance
    private Singleton(){}

    public static Singleton getInstance(){
        return soleInstance;
    }
}
