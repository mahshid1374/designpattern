package singletone.violate_singleton.true_singleton.singleton;

public class Test {
    public static void main(String[] args) {
        //singleton that's not a lot because we made the constructore provide
        //sonobody connot create an Instance of the role
        //Singleton obj = new Singleton();

        //theses two object have the same hash code
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();

        print("s1" ,s1);
        print("s" ,s2);
    }
    static void print(String name,Singleton object){
        System.out.println(String.format("Object : %s, Hashcode : %d",name,object.hashCode()));
    }
}
