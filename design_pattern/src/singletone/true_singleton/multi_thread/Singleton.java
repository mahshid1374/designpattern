package singletone.violate_singleton.true_singleton.multi_thread;

public class Singleton {
    private static Singleton soleInstance = new Singleton();
    private Singleton(){}
    public static Singleton getInstance(){
        return soleInstance;
    }
}
