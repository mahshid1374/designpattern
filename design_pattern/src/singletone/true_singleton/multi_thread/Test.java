package singletone.violate_singleton.true_singleton.multi_thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test {
    static void useSingleton(){
        Singleton s1 = Singleton.getInstance();
        print("Singleton",s1);
    }
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.submit(Test::useSingleton);
        service.submit(Test::useSingleton);
        service.shutdown();
    }
    static void print(String name, Singleton obj){
        System.out.println(String.format("Object %s,Hashcode : %d",name,obj.hashCode()));
    }
}
