package builder.good_state;

public class DetailesAccount {
    //6
    private String fatherFirstName;
    private String country;
    private String city;
    private String job;

    //1
    private DetailesAccount(Builder builder){
        this.fatherFirstName=builder.fatherFirstName;
        this.country=builder.country;
        this.city=builder.city;
        this.job=builder.job;
    }
    //2
    public static class Builder{
        //3
        private String fatherFirstName;
        private String country;
        private String city;
        private String job;
        //4
        public Builder  father_FirstName(final String fatherFirstName){
            this.fatherFirstName=fatherFirstName;
            return this;
        } public Builder  mycountry(final String country){
            this.country=country;
            return this;
        }
        public Builder  mycity(final String city){
            this.city=city;
            return this;
        }
        public Builder  myjob(final String job){
            this.job=job;
            return this;
        }
        //5
        public DetailesAccount build(){
            return new DetailesAccount(this);
        }
    }//end inner class

    @Override
    public String toString() {
        return "DetailesAccount{" +
                "fatherFirstName='" + fatherFirstName + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", job='" + job + '\'' +
                '}';
    }
}
