package builder.good_state;

public class Test {
    public static void main(String[] args) {
        DetailesAccount detailesAccount = new DetailesAccount.Builder()
                .father_FirstName("ali")
                .mycountry("Iran")
                .mycity("Tehran")
                .myjob("java developer")
                .build();
        Account a = new Account.Builder()
                .national_ID(1234)
                .first_Name("Mahshid")
                .last_Name("Pourshoja")
                .email("mahshid@gmail.com")
                .detailes_Account(detailesAccount)
                .build();

        System.out.println(a.toString());
        System.out.println(detailesAccount.toString());
    }
}
