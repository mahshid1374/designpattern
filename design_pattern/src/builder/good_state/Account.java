package builder.good_state;

public class Account {
    private final int nationalID;
    private final String firstName;
    private final String lastName;
    private String email;
    private DetailesAccount detailesAccount;
    //1


    private Account(Builder biulder){
        //5
        this.nationalID=biulder.nationalID;
        this.firstName=biulder.firstName;
        this.lastName=biulder.lastName;
        this.email=biulder.email;
        this.detailesAccount=biulder.detailesAccount;
    }


    //6
    public int getNationalID() { return nationalID; }
    public String getFirstName() { return firstName; }
    public String getLastName() { return lastName; }
    public String getEmail() { return email; }
    public DetailesAccount getDetailesAccount() { return detailesAccount; }

    //2
    public  static class Builder {
        private int nationalID;
        private String firstName;
        private String lastName;
        private String email;
        private DetailesAccount detailesAccount;

        //3 make fluent API
        public Builder national_ID(final int nationalID) {
            this.nationalID = nationalID;
            return this;
        }

        public Builder first_Name(final String firstName){
            this.firstName=firstName;
            return this;
        }
        public Builder last_Name(final String lastName){
            this.lastName=lastName;
            return this;
        }
        public Builder email(final String email){
            this.email=email;
            return this;
        }
        public Builder detailes_Account(final DetailesAccount detailesAccount){
            this.detailesAccount=detailesAccount;
            return this;
        }

        //4
        //for constructor
        public Account build(){
            return new Account(this);
        }
    }//end inner_class

    @Override
    public String toString() {
        return "Account{" +
                "nationalID=" + nationalID +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", detailesAccount=" + detailesAccount +
                '}';
    }
}
