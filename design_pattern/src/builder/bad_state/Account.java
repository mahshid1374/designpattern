package builder.bad_state;
//is pojo
public class Account {
    private int nationalID;
    private String firstName;
    private String lastName;
    private String fatherFirstName;
    private String email;
    private String country;
    private String city;
    private String job;

    public Account(int nationalID, String firstName, String lastName, String fatherFirstName,
                   String email, String country, String city, String job) {
        this.nationalID = nationalID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherFirstName = fatherFirstName;
        this.email = email;
        this.country = country;
        this.city = city;
        this.job = job;
    }

    public int getNationalID() {
        return nationalID;
    }

    public void setNationalID(int nationalID) {
        this.nationalID = nationalID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherFirstName() {
        return fatherFirstName;
    }

    public void setFatherFirstName(String fatherFirstName) {
        this.fatherFirstName = fatherFirstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
}
